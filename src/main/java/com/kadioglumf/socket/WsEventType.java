package com.kadioglumf.socket;

public enum WsEventType {
    TEST_EVENT,
    REFRESH_CONNECTION;
}
