package com.kadioglumf.socket;

public enum RoleType {
    ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN
}

