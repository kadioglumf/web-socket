package com.kadioglumf.socket;

public enum WsSendingType {
    SPECIFIED_USER,
    ROLE_BASED,
    ALL
}
